# Mapping documentation

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## From mapping to database
![4 Steps](img/4step.png)


* __Step 1__: Get correspond product need updating from database by shopee product id


* __Step 2__: Update according to 4 cases    
    * __Case 1__: Crawl product not in cms ==> Insert new product to database   
    
    * __Case 2__: ***Number of rows in crawl*** *=* ***number of rows on cms*** ==> Update respectively from crawl to the database
    <p>&nbsp;</p>

    ![equal mapping](img/equal_mapping.png)
    <p>&nbsp;</p>
    
    ![equal cms](img/equal_cms.png)
    <p>&nbsp;</p>

    * __Case 3__: ***Number of rows in crawl***  *<*  ***number of rows on cms***  ==> Update from oldest row to latest in database
  >  Use row with id 1554, 1555, 1556 in crawl to update row with id 4, 5, 6 in database
    <p>&nbsp;</p>

    ![not equal mapping](img/not_equal_mapping.png)
    <p>&nbsp;</p>
   
    ![not equal cms](img/not_equal_cms.png)
    <p>&nbsp;</p>

    * __Case 4__ : ***Number of rows in crawl***  *>*  ***number of rows on cms***  ==> Update respectively from crawl to the database and insert new product 